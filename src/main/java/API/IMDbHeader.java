package API;

import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.specification.RequestSpecification;
import utils.CredentialPropertiesValue;

import java.util.ArrayList;
import java.util.List;

public class IMDbHeader {

    public static RequestSpecification Request;

    public static Headers getIMDbHeader() {
        Header header1 = new Header("x-rapidapi-key", CredentialPropertiesValue.getPropValues());
        List<Header> list = new ArrayList<Header>();
        list.add(header1);

        return new Headers(list);
    }

}
