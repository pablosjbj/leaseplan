package API;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

public class RestAssuredExtension implements IRestClientLibrary {

    public static RequestSpecification Request;

    public RestAssuredExtension() {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
        requestSpecBuilder.setBaseUri("https://imdb8.p.rapidapi.com/");
        requestSpecBuilder.setContentType(ContentType.JSON);
        RequestSpecification requestSpec = requestSpecBuilder.build();
        Request = RestAssured.given().spec(requestSpec)
                .headers(IMDbHeader.getIMDbHeader());
    }

    @Override
    public Response Post(Object body, String path) throws URISyntaxException {
        Request.body(body);
        return Request.post(path);
    }

    @Override
    public Response Get(String path) throws URISyntaxException {
        return Request.get(new URI(path));
    }

    @Override
    public Response GetWithParam(String path, HashMap param) throws URISyntaxException {
        Request.queryParams(param);
        return Request.get(path);
    }
}
