package API;

import io.restassured.response.Response;

import java.net.URISyntaxException;
import java.util.HashMap;

public class AutoCompleteAPI {

    private IRestClientLibrary restClient;

    public AutoCompleteAPI() {
        restClient = new RestAssuredExtension();
    }

    public Response getAutoCompleteWithValue(String endpoint, String value) throws URISyntaxException {
        HashMap<String, String> pathParams = new HashMap<>();
        pathParams.put("q", value);
        return restClient.GetWithParam(endpoint, pathParams);
    }
}
