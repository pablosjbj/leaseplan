package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CredentialPropertiesValue {

    public static String getPropValues() {

        try (InputStream input = new FileInputStream("src/main/resources/credentials/profile.properties")) {
            Properties prop = new Properties();

            prop.load(input);

            return prop.getProperty("apiToken");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
