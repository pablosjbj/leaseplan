package stepsDefinitions;

import API.AutoCompleteAPI;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.junit.Assert;

import java.net.URISyntaxException;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class stepDefinitions {

    private static Response response;
    private static String stringValue;

    @Given("^I perform GET operation for \"([^\"]*)\" with the value \"([^\"]*)\"$")
    public void iPerformGETOperationForWithTheValue(String endpoint, String value) throws URISyntaxException {
        AutoCompleteAPI autoCompleteAPI = new AutoCompleteAPI();
        stringValue = value;
        response = autoCompleteAPI.getAutoCompleteWithValue(endpoint, value);
    }

    @And("The response status code should be \"([^\"]*)\"$")
    public void theResponseStatusCodeShouldBe(int statusCode) {
        assertThat(response.getStatusCode(), equalTo(statusCode));
    }

    @Then("^The response should be contains store \"([^\"]*)\" like \"([^\"]*)\"$")
    public void theResponseShouldBeContainsStoreLike(String key, String value) {
        String storeValue = response.jsonPath().getString(key);
        Assert.assertEquals("The response not contains the store required.", value, storeValue);
    }

    @And("^The response (is|is not) empty$")
    public void theResponseEmpty(String value) {
        if (value.contains("is")) {
            assertThat(response.getBody().asString(), notNullValue());
        } else {
            assertThat(response.getBody().asString(), nullValue());
        }
    }

    @Then("^The response body should be contains the query sent$")
    public void theResponseBodyShouldBeContainsTheQuerySent() {
        ResponseBody responseBody = response.getBody();
        String bodyAsString = responseBody.asString();

        Assert.assertTrue(bodyAsString.contains(stringValue));
    }
}
