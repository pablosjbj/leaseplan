@smoke
Feature: GET

  Scenario: Gets Auto complete endpoint
    Given I perform GET operation for "/auto-complete" with the value "Iron Man"
    When The response status code should be "200"
    And The response is not empty
    Then The response body should be contains the query sent
